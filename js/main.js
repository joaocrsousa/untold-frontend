window.onbeforeunload = function() {
    window.scrollTo(0, 0);
};

$(document).ready(function() {

    var $body = $("body"),
        $main = $("main"),
        $footer = $("footer"),
        $menu_toggle = $(".menu-toggle");
    	$skip_intro = $(".skip-intro"),
    	$animated_block = $(".animated-block");

    $menu_toggle.on("click", function(e) {
        $body.toggleClass("menu-open");
        $.scrollLock();
        e.preventDefault();
    });

    $('a[href^="#"]').on('click', function(e) {
        var target = this.hash,
            $target = $(target),
            offset_top = $target.offset().top + parseInt($target.find(".row").css("padding-top").replace("px","")) - 17;
        if ($main.hasClass("intro-active")) {
            $menu_toggle.trigger("click");
            setTimeout(function() {
                $skip_intro.trigger("click");
                setTimeout(function() {
                    $('html, body').stop().animate({
                        'scrollTop': offset_top
                    }, 900);
                }, 500);
            }, 500);
        } else {
            $menu_toggle.trigger("click");
            setTimeout(function() {
                $('html, body').stop().animate({
                    'scrollTop': offset_top
                }, 900);
            }, 500);
        }

        e.preventDefault();
    });

    $(".close-overlay").click(function() {
        $menu_toggle.trigger("click");
    });

    $skip_intro.click(function() {
        $(".intro-mask").addClass("skipped");
        $main.removeClass("intro-active");
    });



    var $swiper_caption = $(".swiper-caption");

    var swiper = new Swiper('.swiper-container', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 15,
        slidesPerView: 1,
        loop: true,
        onSlideChangeStart: function(swiper) {
            $swiper_caption.addClass("invisible");
        },
        onSlideChangeEnd: function(swiper) {
            $caption = $(".swiper-slide").eq(swiper.activeIndex).find("img").attr("alt");
            $swiper_caption.text($caption).removeClass("invisible");
        },
        breakpoints: {
            767: {
                slidesPerView: 1.5,
                spaceBetweenSlides: 10,
                centeredSlides: true
            }
        }
    });

    var waypoint_check_bottom = new Waypoint({
        element: document.getElementById('contact'),
        handler: function(direction) {
            if (direction == "down") {
                $body.addClass("hit-bottom");
            } else {
                $body.removeClass("hit-bottom");
            }

        },
        offset: 'bottom-in-view'
    });


	var waypoints = $animated_block.waypoint({
	  handler: function(direction) {
	    if(direction == "down") {
	    	$(this.element).addClass("visible");
	    }
	  },
	  offset: '95%'
	});

	var waypoints = $animated_block.waypoint({
	  handler: function(direction) {
	    if(direction == "up") {
	    	$(this.element).removeClass("visible");
	    }
	  },
	  offset: '100%'
	});


    $('img.svg').each(function() {
        var $img = $(this),
            img_class = $img.attr('class'),
            img_url = $img.attr('src');

        $.get(img_url, function(data) {
            var $svg = $(data).find('svg');
            if (typeof img_class !== 'undefined') {
                $svg = $svg.attr('class', img_class + ' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);
        }, 'xml');

    });

    //$(".animated-block:in-viewport").isInViewport("background","#F00")
    // .each(function(){
    // 	$(this).addClass("visible");
    // });


});
