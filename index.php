<?php
	$title = "Untold - Unexpected stories of places.";
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php print $title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link type="text/css" rel="stylesheet" href="css/main.css">
	</head>
	<body >
		<header role="banner">
			<nav role="navigation">
				<a class="menu-logo" href="/" title="<?php print $title; ?>">
					<img src="files/logo-menu.svg" alt="<?php print $title; ?>">
				</a>
				<ul>
					<li><a href="#work" title="Work">Work</a></li>
					<li><a href="#about" title="About">About</a></li>
					<li><a href="#contact" title="Contact">Contact</a></li>
				</ul>
				<ul class="language-switcher">
					<li><a href="#pt" title="Work" class="active">Pt</a></li>
					<li class="separator"> ⁄ </li>
					<li><a href="#en" title="About">En</a></li>
				</li>
			</nav>
			<button class="menu-toggle" title="Menu">
			  <span class="bar-container">
			    <span class="bar bar-1"></span>
			    <span class="bar bar-2"></span>
			    <span class="bar bar-3"></span>
			  </span>
			</button>
		</header>
		<main role="main" class="container-fluid intro-active">
			<section id="intro" >
				<div class="video">
					<iframe src="https://player.vimeo.com/video/150268967?title=0&byline=0&portrait=0" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<div class="row">
					<div class="column xs-12 sm-8 sm-offset-4 md-5 md-offset-6">
						<h2>Every place holds a story waiting to be told.<br>We offer a unique perspective of your space by telling its story through moving images.</h2>
					</div>
				</div>
			</section>
			<section id="work">
				<div class="row grid">
					<div class="grid-item column xs-12 sm-8 animated-block fade-in-top">
						<div class="swiper-container">
        			<div class="swiper-wrapper">
								<div class="swiper-slide">
									<img class="img-responsive" src="files/slide-1.jpg" alt="Slide 1">
								</div>
								<div class="swiper-slide">
									<img class="img-responsive" src="files/slide-2.jpg" alt="Slide 2">
								</div>
								<div class="swiper-slide">
									<img class="img-responsive" src="files/slide-3.jpg" alt="Slide 3">
								</div>
							</div>
							<div class="swiper-button-next"></div>
        			<div class="swiper-button-prev"></div>
							<div class="swiper-caption">Slide 1</div>
						</div>
					</div>
					<div class="grid-item align-bottom column xs-12 sm-4 md-3 md-offset-1 animated-block fade-in-top">
						<h3>Our <span class="break-line">Focus</h3>
						<p>Art Direction<br>Storytelling + Storyboard<br>Filmmaking<br>Image capture<br>Post Production</p>
					</div>
			</section>
			<section id="about">
				<div class="row">
					<div class="column xs-12 sm-6 md-4 md-offset-1 animated-block fade-in-left">
						<h2 class="type-offset">We are team of passionate filmmakers and architects.</h2>
					</div>
					<div class="column xs-12 sm-6 md-5 md-offset-2 lg-4 lg-offset-2 animated-block fade-in-left fade-delay-3">
						<p>Founded in 2016, Untold is a cross-functional team, Balancing thinking and practice, the company explores innovative approaches to architectural and urban spaces,  and the technologies that build them.</p>
					</div>
					<div class="column xs-12 md-8 md-offset-4 team animated-block fade-in-top fade-delay-2">
						<img class="img-responsive" src="files/team.jpg" alt="Untold Team">
					</div>
					<div class="column xs-12 sm-6 sm-offset-6 md-3 sm-offset-7 team-description">
						<div class="team-column animated-block fade-in-top">
							<h3>Founders</h3>
							<p>Luís Gomes, Ricardo Vicente<br>&amp; Tiago Coelho</p>
						</div>
						<div class="team-column animated-block fade-in-top fade-delay-1">
							<h3>Collaborators</h3>
							<p>António Manuel &amp; Miguel Alberto</p>
						</div>
					</div>
				</div>
			</section>
			<section id="contact">
				<div class="row">
					<div class="column xs-12 sm-6 md-5 md-offset-1 animated-block fade-in-top">
						<h2 class="type-offset">Have a project in mind?<br>Let’s talk about it. </h2>
					</div>
					<div class="animated-block fade-in-top fade-delay-1">
						<div class="column xs-12 sm-5">
							<p>Feel free to call or send us an e-mail.</p>
							<p><a class="email" href="mailto:hello@untold.pt">hello@untold.pt</a></p>
						</div>
						<div class="column xs-12 sm-5 sm-offset-6">
							<div class="contact-details">
								<p>Largo do Rato nº7 4 - E<br>1800-234 Lisbon</p>
							</div>
							<div class="contact-details">
								<p>+351 963 946 756</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
		<footer role="contentinfo">
			<div class="intro-mask">
				<h2 class="tagline">Unexpected<br>stories of places</h2>
				<span class="skip-intro hidden-xs">Discover ↓</span>
				<span class="skip-intro visible-xs"></span>
			</div>
			<div class="close-overlay"></div>
			<div class="watermark">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="1005.604px" height="69.461px" viewBox="0 0 1005.604 69.461" enable-background="new 0 0 1005.604 69.461" xml:space="preserve" class="img-responsive svg replaced-svg">
					<g>
						<g>
							<g>
								<polygon  opacity="0.1" fill="#565451" points="241.31,21.155 241.31,68.622 245.839,68.622 245.917,24.123    "></polygon>
								<polygon class="highlight" opacity="0.1" fill="#565451" points="241.31,1.041 245.908,1.041 245.917,7.104 295.868,42.405 295.868,1.093 300.405,1.093      300.405,68.622 295.868,68.622 295.868,47.821 241.31,9.236    "></polygon>
							</g>
							<g>
								<rect class="highlight" opacity="0.1" x="422.788" y="14.385" fill="#565451" width="4.448" height="54.194"></rect>
								<rect opacity="0.1" x="398.968" y="1.041" fill="#565451" width="52.168" height="4.089"></rect>
							</g>
							<g>
								<rect opacity="0.1" x="714.206" y="1.041" fill="#565451" width="4.537" height="67.538"></rect>
								<rect class="highlight" opacity="0.1" x="728.005" y="64.493" fill="#565451" width="29.481" height="4.086"></rect>
							</g>
							<g>
								<path class="highlight" opacity="0.1" fill="#565451" d="M907.506,10.337c-6.875-6.197-15.941-9.296-27.196-9.296h-10.021V5.13h10.104    c9.902,0,17.855,2.709,23.871,8.137c6.016,5.416,9.02,12.594,9.02,21.55c0,8.877-3.023,16.051-9.07,21.508    c-6.039,5.445-13.978,8.168-23.82,8.168h-19.365l-0.036-63.452h-4.497v67.538h23.815c11.255,0,20.321-3.086,27.196-9.277    c6.87-6.199,10.305-14.365,10.305-24.485C917.811,24.693,914.376,16.518,907.506,10.337z"></path>
							</g>
							<g>
								<path class="highlight" opacity="0.1" fill="#565451" d="M105.143,64.995c-4.825-0.855-8.875-3.068-12.12-6.674c-4.249-4.711-6.356-10.979-6.356-18.792V1.041     h-4.529v38.487c0,9.056,2.531,16.353,7.6,21.847c4.079,4.451,9.226,7.062,15.406,7.926V64.995z"></path>
								<path opacity="0.1" fill="#565451" d="M132.796,1.041v38.487c0,7.814-2.107,14.082-6.327,18.792c-3.208,3.605-7.245,5.818-12.064,6.674v4.307     c6.171-0.863,11.333-3.475,15.406-7.926c5.069-5.494,7.604-12.791,7.604-21.847V1.041H132.796z"></path>
							</g>
							<g>
								<path class="highlight" opacity="0.1" fill="#565451" d="M554.715,53.68c-9.222-11.689-8.808-28.698,1.66-39.902c5.778-6.164,13.948-9.703,22.405-9.703     c6.243,0,12.223,1.902,17.298,5.369l2.812-2.985C593.029,2.286,586.062,0,578.78,0c-9.581,0-18.824,4.012-25.372,11.003     c-12.007,12.843-12.338,32.421-1.507,45.675L554.715,53.68z"></path>
								<path opacity="0.1" fill="#565451" d="M613.491,33.576c-0.259-7.716-3.009-14.955-7.822-20.815l-2.826,3.021     c4.035,5.095,6.356,11.308,6.575,17.923c0.277,8.198-2.659,15.991-8.249,21.977c-5.864,6.258-13.828,9.717-22.407,9.717     c-6.24,0-12.226-1.908-17.304-5.381l-2.802,2.984c5.851,4.174,12.828,6.459,20.105,6.459c9.577,0,18.833-4.01,25.372-10.988     C610.47,51.696,613.792,42.846,613.491,33.576z"></path>
							</g>
						</g>
					</g>
					</svg>
			</div>
		</footer>
		<script src="js/jquery.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>
